﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Logs
    {
        [Key]
        public int LogsID {  get; set; }

        public int TransactionalID { get; set; }
        public Transactions? Transactions { get; set; }
        public DateOnly LoginDate { get; set; }
        public TimeOnly LoginTime { get; set; }
    }
}
