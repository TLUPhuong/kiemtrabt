﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Accounts
    {
        [Key]
        public int AccountID {  get; set; }

        public int CustomerID {  get; set; }
        public Customer? customer { get; set; }
        public string AccountName { get; set; }
    }
}
