﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalID { get; set; }
        public Employees Employees { get; set; }
        public int EmployeeID {  get; set; }
        public Customer Customer { get; set; }
        public int CustomerID {  get; set; }
        public string Name { get; set; }
    }
}
