﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Reports
    {
        [Key]
        public int ReportID {  get; set; }
        public Accounts? Accounts { get; set; }
        public int AccountID {  get; set; }
        public Logs? Logs { get; set; }
        public int LogsID {  get; set; }

        public Transactions? Transactions { get; set; }
        public int TransactionalID {  get; set; }
        public string ReportName {  get; set; }
        public DateOnly ReportDate { get; set; }
    }
}
